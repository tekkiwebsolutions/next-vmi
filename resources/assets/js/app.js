// app.js

// Imported vue, vue-router and vue-axios, axios libraries for our application.
import Vue from 'vue';

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios);


// Imported components
import App from './components/App.vue';
import Dashboard from './components/Dashboard.vue';
import Orders from './components/Orders.vue';

// Route
const routes = [
	{ name: 'Dashboard', path: '/', component: Dashboard },
	{ name: 'Orders', path: '/recent/orders/', component: Orders }
];

// Setup Vue Router
const router = new VueRouter({ 
	mode: 'history', 
	routes: routes
});
new Vue(Vue.util.extend({ router }, App)).$mount('#app');